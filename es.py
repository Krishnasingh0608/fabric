import base
from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import mongo

env.hosts = []
env.user = "root"
env.key_filename = "/home/krishna/Documents/securebo"
#env.parallel = True
#env.warn_only = True

def tuneos():
    sudo("echo never > /sys/kernel/mm/transparent_hugepage/enabled")
    sudo("echo never > /sys/kernel/mm/transparent_hugepage/defrag")
    sudo("ulimit -l unlimited")
    
def makefilesystem():
    sudo("apt-get -y install numactl sysstat htop glances iotop iftop nmon")
    sudo("apt-get -y install xfsprogs")
    sudo("echo y | mkfs -t xfs /dev/sdc")
#    sudo("blockdev --setra 32 /dev/sdc")
    sudo("mkdir /es")
    sudo("mount /dev/sdc /es")
    with cd("/es"):
        sudo("mkdir data")
        sudo("mkdir logs")

@task
def create():
    base.configure();
    base.disable_transparent_hugepages();
    base.network_tune();
    base.locale_setting();
    tuneos()
    makefilesystem()
#    files.append("/etc/fstab", "/dev/sdc   /es   xfs    noatime,nodiratime  0   0", True)
    
    sudo("apt-get -y install default-jre")
    
    sudo("wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | apt-key add -")
    sudo("echo 'deb http://packages.elastic.co/elasticsearch/1.7/debian stable main' | sudo tee -a /etc/apt/sources.list.d/elasticsearch-1.7.list")

    sudo("apt-get -y update")
    sudo("apt-get install -y elasticsearch")
    sudo("update-rc.d elasticsearch defaults")

    configure();


  
def configure():
    sudo("chown -R elasticsearch.elasticsearch /es")
    with cd("/usr/share/elasticsearch/"):
        sudo("bin/plugin -install royrusso/elasticsearch-HQ")
        sudo("bin/plugin install elasticsearch/elasticsearch-lang-mvel/1.7.0")
        sudo("bin/plugin --url https://github.com/dilshanatwork/elasticsearch-direct-routing-plugin/releases/download/1.0/elasticsearch-direct-routing-plugin-1.0.jar --install elasticsearch-direct-routing-plugin")

    put("files/es/elasticsearch", "/etc/default/elasticsearch", True, True)
    put("files/es/elasticsearch.yml", "/etc/elasticsearch/elasticsearch.yml", True, True)
    
#    sudo("service elasticsearch start")
    


