from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import base

#env.hosts = ["40.83.188.40"]
env.user = "root"
env.warn_only = True
#env.parallel = True
#env.key_filename = "/Users/jeetu/Documents/Pem/prince-betaout.pem"
env.key_filename = "home/krishna/Documents/Pem/securebo"

@task
def setMaster():
    env.hosts = ["40.83.188.40"]
   
@task
def setNode():
    env.hosts = ["40.118.208.159","40.78.29.132"]

@task
def master():
    base.configure();
    base.network_tune();
    base.locale_setting();
    sudo("apt-get update")
    sudo("apt-get install -y apache2 apache2-utils")
    sudo("apt-get install -y libcgi-fast-perl libapache2-mod-fcgid")
    sudo("a2enmod fcgid")
    sudo("apt-get install -y munin")
    put("files/munin/master/munin.conf", "/etc/munin/munin.conf", True, True)
    put("files/munin/master/apache.conf", "/etc/munin/apache.conf", True, True)
    sudo("mkdir /var/www/munin")
    sudo("chown munin:munin /var/www/munin")
    sudo("service apache2 restart")
    sudo("service munin-node restart")
 
@task
def node():
    sudo("apt-get update")
    sudo("apt-get install -y munin-node")
    put("files/munin/node/munin-node.conf", "/etc/munin/munin-node.conf", True, True)
    sudo("service munin-node restart")

@task
def addHostInMaster():
    files.append("/etc/munin/munin.conf", "\n[mongodb17]", True)
    files.append("/etc/munin/munin.conf", "\taddress 10.2.2.20", True)
    files.append("/etc/munin/munin.conf", "\tuse_node_name yes", True)
    sudo("service apache2 restart")
    
        
    

    
   


         

