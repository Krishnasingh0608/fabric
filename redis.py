import base
from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import munin

env.hosts = []
env.user = "root"
#env.warn_only = True
#env.parallel = True
env.key_filename = "/home/krishna/Documents/securebo"

def conf():
    put("files/redis/redis.conf", "/etc/redis/redis.conf", True, True)

def ulimit():
    put("files/redis/default/redis-server", "/etc/default/redis-server", True, True)

def install():
    sudo("add-apt-repository ppa:chris-lea/redis-server")
    sudo("apt-get update")
    sudo("apt-get -y install redis-server")

@task
def create_cluster():
    sudo("apt-get -y install ruby")
    sudo("gem install redis")
    sudo("wget http://download.redis.io/redis-stable/src/redis-trib.rb")
    sudo("chmod +x ./redis-trib.rb")
#    sudo("./redis-trib.rb create 10.2.0.51:7000 10.2.0.52:7000 10.2.0.53:7000")

@task
def create():
    base.configure();
    base.network_tune();
    base.locale_setting();
    munin.node()
    install()
    conf()
    ulimit()
    sudo("service redis-server restart")

