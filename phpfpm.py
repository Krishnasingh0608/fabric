from fabric.api import *
from fabric.contrib import *
from fabric.context_managers import *

@task
def install():
         sudo("apt-get install -y python-software-properties")
         sudo("apt-get install -y php5-fpm")
         ulimit()
         
@task
def ulimit():
         put("files/php5-fpm/init/php5-fpm.conf", "/etc/init/php5-fpm.conf", True, True)
         sudo("service php5-fpm restart")



