from fabric.api import *
from fabric.contrib import *
from fabric.context_managers import *
import base
import munin
import redis


env.hosts = []
env.user = "root"
env.warn_only = True
env.key_filename = "/home/krishna/Documents/securebo"

def makefilesystem():
    sudo("echo y | mkfs -t xfs /dev/sdc")
    sudo("mkdir /data")
    sudo("mount /dev/sdc /data")
    with cd("/data"):
        sudo("mkdir rabbitmq")
        sudo("mkdir rabbitmq/mnesia")
        sudo("mkdir rabbitmq/logs")

def build_dependency_rabbit():
    sudo("apt-get build-dep rabbitmq-server")
    

def rabbitmq_ulimit():
    files.append("/etc/default/rabbitmq-server","ulimit -n 65536",True)
    files.append("/etc/default/rabbitmq-server","ulimit -l unlimited",True)
    files.append("/etc/default/rabbitmq-server","ulimit -S -n 64000",True)
    files.append("/etc/default/rabbitmq-server","ulimit -S -l unlimited",True)
      
def restart_rabitmq():
    sudo("service rabbitmq-server restart")
  
def install_rabitmq():
    sudo("apt-get -y install erlang-base-hipe")
    files.append("/etc/apt/sources.list","deb http://www.rabbitmq.com/debian/ testing main",True)
    sudo("wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc")
    sudo("apt-key add rabbitmq-signing-key-public.asc")
    sudo("apt-get update")
    sudo("apt-get -y install rabbitmq-server")

def change_dir():
    sudo("chown -R rabbitmq.rabbitmq /data")
    put("files/rabbitmq/rabbitmq-env.conf", "/etc/rabbitmq/rabbitmq-env.conf", True, True)
    
@task
def create():
    base.configure();
    base.network_tune();
    base.locale_setting();
    makefilesystem()
#    base.fstab()
    install_rabitmq()
    rabbitmq_ulimit()
    change_dir()
    restart_rabitmq()
    enable_plugins()
    add_user()
    add_vhost()
    rabbitmqadmin()


def enable_plugins():
    sudo("rabbitmq-plugins enable rabbitmq_management")
    sudo("rabbitmq-plugins enable rabbitmq_federation")
    sudo("rabbitmq-plugins enable rabbitmq_federation_management")


def verify():
     sudo("service rabbitmq-server status")
     sudo("rabbitmqctl list_users")
     sudo("rabbitmq-plugins list")

def rabbitmqadmin():
     with cd("/usr/local/bin"):
     	sudo("wget http://hg.rabbitmq.com/rabbitmq-management/raw-file/rabbitmq_v2_8_1/bin/rabbitmqadmin")
     	sudo("chmod 744 rabbitmqadmin")

@task
def add_vhost():
    sudo("rabbitmqctl add_vhost bo-durable")
    sudo("rabbitmqctl add_vhost bo-nf")
    sudo('rabbitmqctl set_permissions -p / fuser ".*" ".*" ".*"')
    sudo('rabbitmqctl set_permissions -p / gauser ".*" ".*" ".*"')
    sudo('rabbitmqctl set_permissions -p bo-durable fuser ".*" ".*" ".*"')
    sudo('rabbitmqctl set_permissions -p bo-durable gauser ".*" ".*" ".*"')
    sudo('rabbitmqctl set_permissions -p bo-nf gauser ".*" ".*" ".*"')
   
@task
def add_user():
    sudo("rabbitmqctl change_password guest guestnewpwd")
    sudo("rabbitmqctl add_user gauser get@#amplify249")
    sudo("rabbitmqctl set_user_tags gauser administrator")
    sudo('rabbitmqctl set_permissions -p / gauser ".*" ".*" ".*"')
    sudo("rabbitmqctl add_user fuser gite9359c7")
    sudo("rabbitmqctl set_user_tags fuser administrator")
    sudo('rabbitmqctl set_permissions -p / fuser ".*" ".*" ".*"')
