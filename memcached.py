from fabric.api import *
from fabric.contrib import *
from fabric.context_managers import *
import base

#env.hosts = ["40.78.29.7"]
env.hosts = ["40.112.141.194"]
env.user = "ubuntu"
#env.key_filename = "/Users/jeetu/Documents/Pem/prince-betaout.pem"
env.key_filename = "~/.ssh/id_rsa"
env.parallel = True

@task
def install():
    base.configure();
    base.network_tune();
    base.locale_setting();
    sudo("apt-get -y update")
    sudo("apt-get install -y memcached")
    sudo("service memcached stop")
    put("files/memcache/memcached.conf", "/etc/memcached.conf", True, True)
    sudo("service memcached start")

