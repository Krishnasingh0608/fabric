from fabric.api import *
from fabric.contrib import *
from fabric.context_managers import *
import base
import munin


env.hosts = []
env.user = "root"
env.warn_only = True
env.key_filename = "/home/krishna/Documents/securebo"


curl -X POST "https://api.digitalocean.com/v2/droplets" \
      -d'{"name":"primarylb","region":"nyc3","size":"512mb","private_networking":true,"image":"ubuntu-14-04-x64","user_data":
"#!/bin/bash

apt-get -y update
apt-get -y install nginx
export HOSTNAME=$(curl -s http://169.254.169.254/metadata/v1/hostname)
export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
echo Droplet: $HOSTNAME, IP Address: $PUBLIC_IPV4 > /usr/share/nginx/html/index.html",
      "ssh_keys":[ 1981809 ]}' \
      -H "Authorization: Bearer 087b72017c5a069afa97cd8d48a4e4198c4cba3afd6c9c5401dce8675a027ea0" \
      -H "Content-Type: application/json"



curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer 087b72017c5a069afa97cd8d48a4e4198c4cba3afd6c9c5401dce8675a027ea0" -d '{"droplet_id": 17649052}' "https://api.digitalocean.com/v2/floating_ips"


sudo apt-get -y install ntp

sudo iptables -A INPUT  -i eth1 -p udp -m multiport --dports 5404,5405,5406 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT  -o eth1 -p udp -m multiport --sports 5404,5405,5406 -m conntrack --ctstate ESTABLISHED -j ACCEPT


sudo apt-get install pacemaker

sudo apt-get install haveged

sudo corosync-keygen
sudo chmod 600 /etc/corosync/authkey

sudo apt-get remove --purge haveged
sudo apt-get clean

sudo vi /etc/corosync/corosync.conf

edit,

totem {
  version: 2
  cluster_name: lbcluster
  transport: udpu
  interface {
    ringnumber: 0
    bindnetaddr: server_private_IP_address
    broadcast: yes
    mcastport: 5405
  }
}

quorum {
  provider: corosync_votequorum
  two_node: 1
}

nodelist {
  node {
    ring0_addr: primary_private_IP_address
    name: primary
    nodeid: 1
  }
  node {
    ring0_addr: secondary_private_IP_address
    name: secondary
    nodeid: 2
  }
}

logging {
  to_logfile: yes
  logfile: /var/log/corosync/corosync.log
  to_syslog: yes
  timestamp: on
}


sudo vi /etc/corosync/service.d/pcmk


service {
  name: pacemaker
  ver: 1
}


sudo vi /etc/default/corosync

START=yes


sudo service corosync start


sudo corosync-cmapctl | grep members


sudo update-rc.d pacemaker defaults 20 01

sudo service pacemaker start


#sudo crm status

#sudo crm_mon

sudo crm configure property stonith-enabled=false


sudo crm configure property no-quorum-policy=ignore

sudo crm configure show


sudo curl -L -o /usr/local/bin/assign-ip http://do.co/assign-ip


sudo chmod +x /usr/local/bin/assign-ip


DO_TOKEN=your_digitalocean_pat /usr/local/bin/assign-ip your_floating_ip droplet_id

#while true; do curl floating_IP_address; sleep 1; done

sudo crm configure clone Nginx-clone Nginx

sudo crm configure colocation FloatIP-Nginx inf: FloatIP Nginx-clone

#sudo crm configure show





curl -X POST "https://api.digitalocean.com/v2/droplets" \
      -d'{"name":["api1","api2"],"region":"nyc3","size":"512mb","private_networking":true,"image":"ubuntu-14-04-x64","user_data":
"#!/bin/bash

apt-get -y update
apt-get -y install nginx
export HOSTNAME=$(curl -s http://169.254.169.254/metadata/v1/hostname)
export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
echo Droplet: $HOSTNAME, IP Address: $PUBLIC_IPV4 > /usr/share/nginx/html/index.html",
      "ssh_keys":[ 1981809 ]}' \
      -H "Authorization: Bearer 087b72017c5a069afa97cd8d48a4e4198c4cba3afd6c9c5401dce8675a027ea0" \
      -H "Content-Type: application/json"


curl 169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address && echo

curl 169.254.169.254/metadata/v1/interfaces/public/0/anchor_ipv4/address && echo


Api server,

sudo vi /etc/nginx/sites-available/default

server {
    listen app_server_private_IP:80;
    allow load_balancer_1_private_IP;
    allow load_balancer_2_private_IP;
    deny all;



Loadbalancer,

sudo service nginx restart


sudo crm resource stop Nginx
sudo crm configure delete Nginx
sudo service nginx stop
sudo apt-get purge nginx
sudo rm -r /etc/nginx


sudo add-apt-repository ppa:vbernat/haproxy-1.6

sudo apt-get update
sudo apt-get install haproxy


sudo vi /etc/haproxy/haproxy.cfg

    option forwardfor
    option http-server-close

frontend http
    bind    load_balancer_anchor_IP:80
    default_backend app_pool


backend app_pool
    server app-1 app_server_1_private_IP:80 check
    server app-2 app_server_2_private_IP:80 check


sudo haproxy -f /etc/haproxy/haproxy.cfg -c


sudo service haproxy restart


while true; do curl floating_IP_address; sleep 2; done