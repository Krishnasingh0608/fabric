from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import base
import nginx
import Dovm

env.hosts = []
env.user = "root"	
#env.key_filename = "/Users/jeetu/Documents/Pem/prince-betaout.pem"
env.key_filename = "/home/krishna/Documents/securebo"

@task
def create():
    base.configure();
    base.network_tune();
    base.locale_setting();
    nginx.install()
    setuplb()
#    install_nginx()
#    install_pacemaker()
#    assign_floatip()
#    install_haveged()
#    pacemaker_configure()
#    remove_nginx()
#    install_haproxy()


def setuplb():
    sudo("rm -rf /etc/nginx/conf.d/*")
    put("files/nginxlb/nginx.conf", "/etc/nginx/nginx.conf", True, True)
    nginx.upload_betaout_in_ssl()
    



def install_nginx():
    sudo("service nginx restart")    
    sudo("apt-get -y update")
    sudo("export HOSTNAME=$(curl -s http://169.254.169.254/metadata/v1/hostname)")
    privateadd = sudo("curl 169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address && echo")
    lbanchorip = sudo("curl 169.254.169.254/metadata/v1/interfaces/public/0/anchor_ipv4/address && echo")
    sudo("export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)")
    sudo("export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)")
    sudo("echo Droplet: $HOSTNAME, IP Address: $PUBLIC_IPV4 > /usr/share/nginx/html/index.html")    
    

def assign_floatip():    
    sudo('curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer 087b72017c5a069afa97cd8d48a4e4198c4cba3afd6c9c5401dce8675a027ea0" -d "{ "droplet_id": "+str(id)+"}" "https://api.digitalocean.com/v2/floating_ips"')


def install_haveged():
    sudo("apt-get install haveged")
    sudo("sudo corosync-keygen")
    sudo("sudo apt-get remove --purge haveged")
    sudo("sudo apt-get clean")


def pacemaker_configure():
	sudo("apt-get install pacemaker")
	put("files/corosync/corosync.conf", "/etc/corosync/corosync.conf", True, True)
	files.append("/etc/corosync/corosync.conf","nodelist {node {ring0_addr: privateadd name: primary nodeid: 1 } }",True)
   	files.append("/etc/corosync/service.d/pcmk", "service {name: pacemaker ver: 1}", True)
   	put("files/default/corosync", "/etc/default/corosync", True, True)
   	sudo("service corosync start")
  	sudo("sudo update-rc.d pacemaker defaults 20 01")
   	sudo("service pacemaker start")
  	sudo("sudo crm configure property stonith-enabled=false")
  	sudo("sudo crm configure property no-quorum-policy=ignore")
#   sudo("sudo curl -L -o /usr/local/bin/assign-ip http://do.co/assign-ip")
#   sudo("sudo chmod +x /usr/local/bin/assign-ip")
#    sudo("DO_TOKEN=087b72017c5a069afa97cd8d48a4e4198c4cba3afd6c9c5401dce8675a027ea0 /usr/local/bin/assign-ip 138.197.60.124 17649052")
#    sudo("sudo mkdir /usr/lib/ocf/resource.d/digitalocean")
#    sudo("sudo curl -o /usr/lib/ocf/resource.d/digitalocean/floatip https://gist.githubusercontent.com/thisismitch/b4c91438e56bfe6b7bfb/raw/2dffe2ae52ba2df575baae46338c155adbaef678/floatip-ocf")
#    sudo("sudo chmod +x /usr/lib/ocf/resource.d/digitalocean/floatip")


def remove_nginx():
    sudo("sudo service nginx stop")
    sudo("sudo apt-get purge nginx")
    sudo("sudo rm -r /etc/nginx")


def install_haproxy():    
    sudo("sudo add-apt-repository ppa:vbernat/haproxy-1.6")
    sudo("sudo apt-get update")
    sudo("sudo apt-get install haproxy")
    files.append("/etc/haproxy/haproxy.cfg", "option forwardforoption http-server-close", True)
    put("files/haproxy/haproxy.cfg", "/etc/haproxy/haproxy.cfg", True, True)
    sudo("sudo service haproxy restart")
#   sudo("cd /usr/lib/ocf/resource.d/heartbeat")
#   sudo("sudo curl -O https://raw.githubusercontent.com/thisismitch/cluster-agents/master/haproxy")
#    sudo("sudo chmod +x haproxy")
#    sudo("sudo crm configure primitive haproxy ocf:heartbeat:haproxy op monitor interval=15s")
#    sudo("sudo crm configure colocation FloatIP-haproxy inf: FloatIP haproxy-clone")







