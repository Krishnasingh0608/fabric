import base
from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import munin
import mysql


env.hosts = []
env.user = "root"
env.key_filename = "/home/krishna/Documents/securebo"
#env.parallel = True
#env.warn_only = True

#cat /sys/block/sdd/queue/scheduler
# should be deadline

@task
def temp():
    put("~/Documents/Pem/id_rsa", "/home/ubuntu/id_rsa", True, True)
    
@task
def temp1():
    sudo("service mongod stop")
    sudo("rm -rf /data/db/*")
    sudo("echo ''> /mnt/log/mongod.log")
    
def tuneos():
    sudo("echo never > /sys/kernel/mm/transparent_hugepage/enabled")
    sudo("echo never > /sys/kernel/mm/transparent_hugepage/defrag")
    
def makefilesystem():
    sudo("apt-get -y install numactl sysstat htop glances iotop")
    sudo("apt-get -y install xfsprogs")
    sudo("echo y | mkfs -t xfs /dev/sdc")
    sudo("mkdir /data")
    sudo("mount /dev/sdc /data")
    with cd("/data"):
        sudo("mkdir db")
        sudo("mkdir log")

 
def stripe_filesystem():
    sudo("apt-get -y install numactl sysstat htop glances")
    sudo("apt-get -y install mdadm")
    sudo("apt-get -y install xfsprogs")
    sudo("mdadm --create /dev/md1 --level 0 --raid-devices 8 /dev/sdc /dev/sdd /dev/sde /dev/sdf /dev/sdg /dev/sdh /dev/sdi /dev/sdj")
    sudo("mkfs -t xfs /dev/md1")
    sudo("mkdir /data")
    sudo("mount /dev/md1 /data/")
    with cd("/data"):
        sudo("mkdir db")
    with cd("/mnt"):
        sudo("mkdir log")
 
@task
def create(serverType="datanode"):
    base.configure();
    base.disable_transparent_hugepages();
    base.network_tune();
    base.locale_setting();
    tuneos()
    makefilesystem()
#    stripe_filesystem()
    sudo("apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10")
    distribution = sudo("echo $(lsb_release -sc)")
    print distribution
    pckg = "deb http://repo.mongodb.org/apt/ubuntu " + distribution + "/mongodb-org/3.0 multiverse"
    print pckg
    files.append("/etc/apt/sources.list.d/mongodb-org-3.0.list", pckg, True)
    sudo("apt-get -y update")
    sudo("apt-get install -y mongodb-org")
#    sudo("apt-get install -y mongodb-org=3.0.0 mongodb-org-server=3.0.0 mongodb-org-shell=3.0.0 mongodb-org-mongos=3.0.0 mongodb-org-tools=3.0.0")
    configure(serverType);
    #setreadahead()
    

def configure(serverType):
    sudo("chown -R mongodb.mongodb /data")
    if serverType == "datanode":
        print "Data Node Case"
        put("files/mongo/datanode/etc/mongod.conf", "/etc/mongod.conf", True, True)
    else:
        print "Config Node Case"
        put("files/mongo/configdb/mongod.conf", "/etc/mongod.conf", True, True)
    put("files/mongo/datanode/init/mongod.conf", "/etc/init/mongod.conf", True, True)
    sudo("service mongod restart")
    
def setreadahead():
    sudo("blockdev --setra 32 /dev/sdc")
    

def munin_mongo_plugin():
    sudo("apt-get -y install git")
    sudo("apt-get -y install python-pip")
    sudo("apt-get -y install pip")
    sudo("apt-get install build-essential python-dev")
    sudo("pip install pymongo")
    sudo("git clone https://github.com/comerford/mongo-munin.git /tmp/mongo-munin")
    sudo("cp /tmp/mongo-munin/mongo_* /usr/share/munin/plugins")
    sudo("ln -sf /usr/share/munin/plugins/mongo_btree /etc/munin/plugins/mongo_btree")
    sudo("ln -sf /usr/share/munin/plugins/mongo_conn /etc/munin/plugins/mongo_conn")
    sudo("ln -sf /usr/share/munin/plugins/mongo_lock /etc/munin/plugins/mongo_lock")
    sudo("ln -sf /usr/share/munin/plugins/mongo_mem /etc/munin/plugins/mongo_mem")
    sudo("ln -sf /usr/share/munin/plugins/mongo_ops /etc/munin/plugins/mongo_ops")
    sudo("ln -sf /usr/share/munin/plugins/mongo_docs /etc/munin/plugins/mongo_docs")
    sudo("chmod +x /usr/share/munin/plugins/mongo_*")
    sudo("service munin-node restart")

