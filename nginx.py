from fabric.api import *
from fabric.contrib import *
from fabric.context_managers import *

def prepare():
    sudo("wget http://nginx.org/keys/nginx_signing.key")
    sudo("apt-key add nginx_signing.key")
    distribution = sudo("echo $(lsb_release -sc)")
    print distribution
    filename = "/etc/apt/sources.list.d/nginx-stable.list"
    pckg1 = "deb http://nginx.org/packages/ubuntu/ " + distribution + " nginx"
    pckg2 = "deb-src http://nginx.org/packages/ubuntu/ " + distribution + " nginx"
    print pckg1
    print pckg2
    files.append(filename, pckg1, True)
    files.append(filename, pckg2, True)

@task
def install():
    prepare()
    sudo("apt-get -y update")
    sudo("apt-get install -y nginx")
    ulimit()
         
@task
def ulimit():
    put("files/nginx/default/nginx", "/etc/default/nginx", True, True)
    sudo("service nginx restart")
         
@task
def tuneapi():
    sudo("service nginx stop")
    sudo("service php5-fpm stop")
    sudo("rm -rf /etc/nginx/conf.d/*")
    put("files/nginx/nginx.conf", "/etc/nginx/nginx.conf", True, True)
    put("files/nginx/conf.d/20769-api.conf", "/etc/nginx/conf.d/20769-api.conf", True, True)  
    put("files/nginx/conf.d/betaout-in-api.conf", "/etc/nginx/conf.d/betaout-in-api.conf", True, True)  
    put("files/nginx/conf.d/betaout-com-api.conf", "/etc/nginx/conf.d/betaout-com-api.conf", True, True)  
    put("files/nginx/conf.d/default.conf", "/etc/nginx/conf.d/default.conf", True, True)  
    put("files/nginx/conf.d/email-tracking.conf", "/etc/nginx/conf.d/email-tracking.conf", True, True)  
    put("files/nginx/conf.d/example_ssl.conf", "/etc/nginx/conf.d/example_ssl.conf", True, True)  
    put("files/nginx/conf.d/ip.conf", "/etc/nginx/conf.d/ip.conf", True, True)
    put("files/nginx/conf.d/shorten.conf", "/etc/nginx/conf.d/shorten.conf", True, True) 
    put("files/nginx/conf.d/staging-api.conf", "/etc/nginx/conf.d/staging-api.conf", True, True)
    put("files/nginx/conf.d/www.conf", "/etc/php5/fpm/pool.d/www.conf", True, True)
    put("files/nginx/conf.d/betaout-com-api.conf", "/etc/nginx/conf.d/betaout-com-api.conf", True, True) 
    put("files/nginx/conf.d/betaout-com-api-ssl.conf", "/etc/nginx/conf.d/betaout-com-api-ssl.conf", True, True) 
    sudo("service nginx start")
    sudo("service php5-fpm start")
        
@task
def tunefront():
    sudo("service nginx stop")
    sudo("service php5-fpm stop")
    sudo("rm -rf /etc/nginx/conf.d/*")
    put("files/nginx/front/nginx.conf", "/etc/nginx/nginx.conf", True, True)
    put("files/nginx/front/conf.d/betaout-com.conf", "/etc/nginx/conf.d/betaout-com.conf", True, True)  
#    put("files/nginx/front/conf.d/betaout.conf", "/etc/nginx/conf.d/betaout.conf", True, True)  
    put("files/nginx/front/conf.d/default.conf", "/etc/nginx/conf.d/default.conf", True, True)  
#        put("files/nginx/front/conf.d/demo.conf", "/etc/nginx/conf.d/demo.conf", True, True)  
#        put("files/nginx/front/conf.d/demo1.conf", "/etc/nginx/conf.d/demo1.conf", True, True)  
#    put("files/nginx/front/conf.d/example_ssl.conf", "/etc/nginx/conf.d/example_ssl.conf", True, True)  
#    put("files/nginx/front/conf.d/kartopt.conf", "/etc/nginx/conf.d/kartopt.conf", True, True)
    put("files/nginx/front/conf.d/phpmyadmin.conf", "/etc/nginx/conf.d/phpmyadmin.conf", True, True) 
#    put("files/nginx/front/conf.d/rockmongo.conf", "/etc/nginx/conf.d/rockmongo.conf", True, True)
    put("files/nginx/front/conf.d/betaout-com-ssl.conf", "/etc/nginx/conf.d/betaout-com-ssl.conf", True, True)
#        put("files/nginx/front/conf.d/staging.conf", "/etc/nginx/conf.d/staging.conf", True, True)
    put("files/nginx/front/conf.d/www.conf", "/etc/php5/fpm/pool.d/www.conf", True, True)
    sudo("service nginx start")
    sudo("service php5-fpm start")

@task
def setup_betaout_com_ssl():
    sudo("mkdir /etc/nginx/ssl")
    sudo("mkdir /etc/nginx/ssl/betaout.com")
    put("files/new-wildcard-betaout-com-ssl/public.key", "/etc/nginx/ssl/betaout.com/public.key", True, True)
    put("files/new-wildcard-betaout-com-ssl/private.pem", "/etc/nginx/ssl/betaout.com/private.pem", True, True)
    put("files/new-wildcard-betaout-com-ssl/inter-root", "/etc/nginx/ssl/betaout.com/inter-root", True, True)
    put("files/new-wildcard-betaout-com-ssl/pub-inter-root.key", "/etc/nginx/ssl/betaout.com/pub-inter-root.key", True, True)
    sudo("service nginx restart")
    
@task
def setup_betaout_com_staging_ssl():
    put("files/nginx/front/conf.d/staging-ssl.conf", "/etc/nginx/conf.d/staging-ssl.conf", True, True)
    sudo("service nginx restart")
   
@task
def upload_old_betaout_in_ssl():
    sudo("mkdir /etc/nginx/ssl")
    sudo("mkdir /etc/nginx/ssl/betaout.in")
    put("files/wildcard-betaout-in-ssl/public.key", "/etc/nginx/ssl/betaout.in/public.key", True, True)
    put("files/wildcard-betaout-in-ssl/private.pem", "/etc/nginx/ssl/betaout.in/private.pem", True, True)
    put("files/wildcard-betaout-in-ssl/inter-root", "/etc/nginx/ssl/betaout.in/inter-root", True, True)
    put("files/wildcard-betaout-in-ssl/pub-inter-root.key", "/etc/nginx/ssl/betaout.in/pub-inter-root.key", True, True)
    
@task
def upload_betaout_in_ssl():
    sudo("mkdir /etc/nginx/ssl")
    sudo("mkdir /etc/nginx/ssl/betaout.in")
    put("files/new-wildcard-betaout-in-ssl/private.pem", "/etc/nginx/ssl/betaout.in/private.pem", True, True)
    put("files/new-wildcard-betaout-in-ssl/pub-inter-root.key", "/etc/nginx/ssl/betaout.in/pub-inter-root.key", True, True)
    
@task
def setup_betaout_in_ssl():
    upload_betaout_in_ssl()
    put("files/nginx/conf.d/betaout-in-api-ssl.conf", "/etc/nginx/conf.d/betaout-in-api-ssl.conf", True, True)
    sudo("service nginx restart")

