from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import base
import rabbitmq

env.hosts = []
env.user = "root"
env.warn_only = True
env.key_filename = "/home/krishna/Documents/securebo"

def makefilesystem():
    sudo("apt-get -y install xfsprogs")
    sudo("echo y | xfs -t ext4 /dev/sdc")
    sudo("mkdir /data")
    sudo("mount /dev/sdc /data")
    with cd("/data"):
        sudo("mkdir mysql")
        sudo("mkdir log")
        sudo("mkdir bin-log")
    files.append("/etc/fstab", "/dev/sdc   /data   xfs    noatime,nodiratime  0   0", True)

@task
def create(type):
    base.configure();
    base.network_tune();
    base.locale_setting();
    makefilesystem()
    sudo("export DEBIAN_FRONTEND=noninteractive")
    sudo("apt-get -y -q install mysql-server-5.6 mysql-client-5.6")
    sudo("service mysql stop")
    sudo("mv /var/lib/mysql/* /data/mysql/")
    put("files/mysql/master/etc/init/mysql.conf", "/etc/init/mysql.conf", True, True)
    put("files/mysql/master/usr.sbin.mysqld", "/etc/apparmor.d/usr.sbin.mysqld", True, True)
    sudo("chown -R mysql.mysql /data")
    tune(type)
    sudo("service mysql start")

    
@task
def tune(type):
    if type == "master":
        print "master mysql Case"
        put("files/mysql/master/my.cnf", "/etc/mysql/my.cnf", True, True)
    else:
        print "slave mysql Case"
        put("files/mysql/slave/my.cnf", "/etc/mysql/my.cnf", True, True)

    
    


         
