from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *


@task    
def replace_pem():
    sudo("echo ''> /.ssh/authorized_keys")
    pubfile = local("cat ~/Documents/securerepo.pub", capture=True)
    print type(pubfile), pubfile
    files.append("/home/krishna/.ssh/authorized_keys", pubfile, True)
    sudo("service ssh restart")
   

def update():
    run("apt-get update", shell=False)

def set_date():
    run("rm -rf /etc/localtime",shell=False)
    sudo("ln -sf /usr/share/zoneinfo/GMT /etc/localtime", shell=False)

def ntp():
    sudo("apt-get -y install ntp")
    
def monitoring_tool():
    sudo("apt-get -y install numactl sysstat htop glances iotop iftop xfsprogs")

@task
def configure():
    set_date()
    update()
    ntp()
    monitoring_tool()


@task
def php5_redis():
    sudo("apt-get -y install php5-redis")    
    
@task
def disable_transparent_hugepages():
    put("files/mongo/disable-transparent-hugepages", "/etc/init.d/disable-transparent-hugepages", True, True)
    sudo("chmod 755 /etc/init.d/disable-transparent-hugepages")
    sudo("update-rc.d disable-transparent-hugepages defaults")
    
@task
def tcp_keepalive():
    files.append("/etc/sysctl.conf", "net.ipv4.tcp_keepalive_time = 200", True)
    files.append("/etc/sysctl.conf", "net.ipv4.tcp_keepalive_intvl = 200", True)
    files.append("/etc/sysctl.conf", "net.ipv4.tcp_keepalive_probes = 5", True)
    sudo("/sbin/sysctl -w net.ipv4.tcp_keepalive_time=200 net.ipv4.tcp_keepalive_intvl=200 net.ipv4.tcp_keepalive_probes=5")
    
@task    
def installmongos():
    sudo("apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10")
    distribution = sudo("echo $(lsb_release -sc)")
    print distribution
    pckg = "deb http://repo.mongodb.org/apt/ubuntu " + distribution + "/mongodb-org/3.0 multiverse"
    print pckg
    files.append("/etc/apt/sources.list.d/mongodb-org-3.0.list", pckg, True)
    sudo("apt-get -y update")
    sudo("apt-get install -y mongodb-org")
    sudo("service mongod stop")
    put_mongos_conf()
    put("files/mongos/etc/init/mongod.conf", "/etc/init/mongod.conf", True, True)
    sudo("service mongod start")

@task    
def put_mongos_conf():
    put("files/mongos/etc/mongod.conf", "/etc/mongod.conf", True, True)

@task    
def installmtools():
    sudo("apt-get -y install python-pip")
    sudo("apt-get -y install python-dev")
    sudo("pip install mtools")
#    mloginfo /data/log/mongod.log --queries
  
@task    
def installsupervisor():
    sudo("apt-get -y install supervisor")
    
@task    
def installsubversion():
    sudo("apt-get -y install subversion")
    
@task    
def locale_setting():
    files.append("/etc/bash.bashrc", "export LC_ALL=C", True)
    
@task    
def network_tune():
    put("files/sysctl.conf", "/etc/sysctl.conf", True, True)
    sudo("sysctl -p")
  
@task
def phpbasic():
    sudo("apt-get -y install php-pear")
    sudo("pear install Crypt_HMAC Crypt_HMAC2 HTTP_Request2")
    sudo("apt-get -y install php5-mcrypt") 
    sudo("php5enmod mcrypt")

@task
def phpmodules():
    sudo("apt-get -y install php5-dev php5 php5-curl php5-mysql php5-gd php5-apcu php5-memcached php5-redis")
    sudo("pecl install mongo")
    sudo("echo '' > /etc/php5/mods-available/mongo.ini")
    files.append("/etc/php5/mods-available/mongo.ini", "extension=mongo.so", True)
    sudo("php5enmod mongo")

@task
def rmqclient():
    with cd("~"):
        sudo("rm -rf rabbit*")
        sudo("wget https://github.com/alanxz/rabbitmq-c/releases/download/v0.6.0/rabbitmq-c-0.6.0.tar.gz")
        sudo("tar -xvzf rabbitmq-c-0.6.0.tar.gz")
        sudo("cd rabbitmq-c-0.6.0")
        sudo("~/rabbitmq-c-0.6.0/configure")
        sudo("make")
        sudo("make install")
        sudo("pecl install https://pecl.php.net/get/amqp-1.6.0beta4.tgz")
        sudo("echo '' > /etc/php5/mods-available/amqp.ini")
        files.append("/etc/php5/mods-available/amqp.ini", "extension=amqp.so", True)
        sudo("php5enmod amqp")
        sudo("rm -rf rabbit*")


@task
def twemproxy():
    with cd("~"):
        sudo("apt-get -y install git")
        sudo("git clone https://github.com/twitter/twemproxy.git")
        with cd("/home/ubuntu/twemproxy"):
            sudo(" autoreconf -fvi")
            sudo("./configure")
            sudo("make")
            sudo("make install")
            sudo("mv /home/ubuntu/twemproxy/conf/nutcracker.yml /home/ubuntu/")
            put("files/nutcracker.yml", "/home/ubuntu/twemproxy/conf/nutcracker.yml", True, True)
            sudo("./src/nutcracker -c conf/nutcracker.yml -d")


@task
def set_timezone():
    files.append("/etc/php5/cli/php.ini", 'date.timezone = "UTC"', True)
    files.append("/etc/php5/fpm/php.ini", 'date.timezone = "UTC"', True)
    sudo("service php5-fpm restart")
    
@task
def disable_warning():
    disable_phpcli_warning()
    disable_phpfpm_warning()
    
@task
def install_predis():
    sudo("pear channel-discover pear.nrk.io")
    sudo("pear install nrk/predis")
    
    
@task
def disable_phpcli_warning():
    files.append("/etc/php5/cli/php.ini", "error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE & ~E_WARNING", True)

@task
def disable_phpfpm_warning():
    files.append("/etc/php5/fpm/php.ini", "error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE & ~E_WARNING", True)
    sudo("service php5-fpm restart")
    
@task
def upload_size():
    files.append("/etc/php5/fpm/php.ini", "post_max_size = 30M", True)
    files.append("/etc/php5/fpm/php.ini", "upload_max_filesize = 60M", True)
    sudo("service php5-fpm restart")

@task
def fstab():
    files.append("/etc/fstab", "/dev/sdc   /data   ext4    noatime,nodiratime  0   0", True)

@task
def enablemysqlreconnect():
    files.append("/etc/php5/cli/php.ini", "mysqli.reconnect = On", True)
    files.append("/etc/php5/fpm/php.ini", "mysqli.reconnect = On", True)

@task
def checkout():
    with cd("/var/www"):
        installsubversion()
        sudo("svn co https://betaout.sourcerepo.com/betaout/parmanu/tags/jal/")
        sudo("svn co https://betaout.sourcerepo.com/betaout/foundation")  
        
@task
def remove_ubuntu_from_sudo():
    sudo("mv /etc/sudoers.d/90-cloud-init-users /home/ubuntu")
    
@task
def php_gettext():
    sudo("apt-get install php-gettext")
    
@task
def php_benchmark():
    sudo("pear install benchmark")
    
@task
def remove_apache2():
    sudo("apt-get remove apache2")
  
@task
def setpwd():
    sudo("passwd ubuntu")
    
@task
def copypem():
    put("/Users/jeetu/Documents/Pem/id_rsa", "/home/ubuntu/id_rsa", True, True)
    
@task   
def custom(command):
    sudo(command)
    


