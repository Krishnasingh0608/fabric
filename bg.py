import base
from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import front

env.hosts = []
env.user = "root"
env.key_filename = "/home/krishna/Documents/securebo"

def phpbasic():
    sudo("apt-get -y install php-pear")
    sudo("pear install Crypt_HMAC Crypt_HMAC2 Benchmark HTTP_Request HTTP_Request2")
    sudo("apt-get -y install php5-mcrypt") 
    sudo("php5enmod mcrypt")

def phpmodules():
    sudo("apt-get -y install php5-dev php5 php5-curl php5-mysql php5-gd php5-apcu php5-memcached")
    sudo("pecl install mongo")
    sudo("echo '' > /etc/php5/mods-available/mongo.ini")
    files.append("/etc/php5/mods-available/mongo.ini", "extension=mongo.so", True)
    sudo("php5enmod mongo")

def rmqclient():
    with cd("~"):
        sudo("rm -rf rabbit*")
        sudo("wget https://github.com/alanxz/rabbitmq-c/releases/download/v0.6.0/rabbitmq-c-0.6.0.tar.gz")
        sudo("tar -xvzf rabbitmq-c-0.6.0.tar.gz")
        sudo("cd rabbitmq-c-0.6.0")
        sudo("~/rabbitmq-c-0.6.0/configure")
        sudo("make")
        sudo("make install")
        sudo("pecl install https://pecl.php.net/get/amqp-1.6.0beta4.tgz")
        sudo("echo '' > /etc/php5/mods-available/amqp.ini")
        files.append("/etc/php5/mods-available/amqp.ini", "extension=amqp.so", True)
        sudo("php5enmod amqp")
        sudo("rm -rf rabbit*")

def cleanhome():
    with cd("~"):
        sudo("rm -rf Makefile")
        sudo("rm -rf config.h")
        sudo("rm -rf config.log")
        sudo("rm -rf config.status")
        sudo("rm -rf examples")
        sudo("rm -rf librabbitmq")
        sudo("rm -rf librabbitmq.pc")
        sudo("rm -rf libtool")
        sudo("rm -rf stamp-h1")
        sudo("rm -rf tests")
        sudo("rm -rf tools")
        
def enablemysqlreconnect():
    files.append("/etc/php5/cli/php.ini", "mysqli.reconnect = On", True)

def rmmongos():
    sudo("rm -rf /etc/init.d/mongos")
    sudo("rm -rf /etc/mongod.conf")
    sudo("rm -rf /data")
    sudo("apt-get -y remove mongodb-org")
     
        
def checkout():
    with cd("/var/www"):
        sudo("apt-get -y install subversion")
        sudo("svn co https://betaout.sourcerepo.com/betaout/parmanu/tags/jal/")
        sudo("svn co https://betaout.sourcerepo.com/betaout/foundation")

@task
def create():
    base.configure();
    base.network_tune();
    base.locale_setting();
    phpbasic()
    phpmodules()
    rmqclient()
    enablemysqlreconnect()
    checkout()
    base.installsupervisor()
    base.installmongos()
    base.php5_redis()
    base.install_predis()
    base.disable_phpcli_warning()
    cleanhome() 


