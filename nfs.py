import base
from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import munin

env.hosts = ["40.78.66.138"]
env.user = "ubuntu"
env.key_filename = "~/Documents/Pem/securebo"
env.parallel = False
env.warn_only = True

def tuneos():
    sudo("echo never > /sys/kernel/mm/transparent_hugepage/enabled")
    sudo("echo never > /sys/kernel/mm/transparent_hugepage/defrag")
    sudo("ulimit -l unlimited")
    
def makefilesystem():
    sudo("echo y | mkfs -t xfs /dev/sdc")
    sudo("mkdir /backup")
    sudo("mount /dev/sdc /backup")

@task
def install():
    base.configure();
    base.disable_transparent_hugepages();
    base.network_tune();
    base.locale_setting();
    base.monitoring_tool();
    tuneos()
    makefilesystem();
    nfsconfigure();
  
def nfsconfigure():
    sudo("apt-get -y install nfs-kernel-server portmap");
    
    
