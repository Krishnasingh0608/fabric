from fabric.api import *
from fabric.context_managers import *
from fabric.contrib import *
import base
import nginx
import phpfpm
import es

env.hosts = []
env.user = "root"
env.warn_only = True
env.key_filename = "/home/krishna/Documents/securebo"


def checkout_other_components():
    with cd("/var/www"):
        sudo("svn co https://betaout.sourcerepo.com/betaout/bostatic demo")
        sudo("svn co https://betaout.sourcerepo.com/betaout/demo demo1")


def staging_setup():
    run("mkdir /home/ubuntu/staging")
    with cd("/home/ubuntu/staging"):
        run("svn co https://betaout.sourcerepo.com/betaout/parmanu/tags/jal/")
        run("svn co https://betaout.sourcerepo.com/betaout/foundation") 

@task
def install_phpmyadmin():
    sudo("apt-get install phpmyadmin")
    
@task
def create():
    base.configure();
    base.network_tune();
    base.locale_setting();
    base.setpwd();
    base.phpbasic()
    base.phpmodules()
    base.install_predis()
    base.rmqclient()
    base.enablemysqlreconnect()
    base.php_gettext()
    base.php_benchmark()
#    staging_setup()
    base.checkout()
#    checkout_other_components()
    base.installmongos()
    base.remove_apache2()
    nginx.install()
    phpfpm.install()
    nginx.setup_betaout_com_ssl()
    nginx.tunefront()
#    nginx.setup_betaout_com_staging_ssl()
#    base.remove_ubuntu_from_sudo()
    

 


         

  
